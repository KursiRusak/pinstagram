<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
use App\Models\Auth;

class LikeController extends Controller
{
    public function getLike($id)
    {
        $like = Like::with('user')->where('post_id', $id)->get();
        return $like;
    }

    public function toggleLike(Request $request)
    {
        $like = Like::where('post_id', $request->input('post_id'))
                    ->where('user_id', $request->input('user_id'))
                    ->first();
        if(!$like)
        {
            $like = new Like();
            $like->post_id = $request->input('post_id');
            $like->user_id = $request->input('user_id');
            $like->save();
            return ['status' => 'OK', 'data' => 'like'];
        } 
        else 
        {
            $like->delete();
            return ['status' => 'OK', 'data' => 'dislike'];
        }

    }
}
