<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use App\Models\Follower;
use App\Models\Following;

class UserController extends Controller
{
    // return user profile page
    public function index($id)
    {
        $data['user'] = $id;
        return view('user')->with($data);
    }

    // return user object
    public function getById($id)
    {
        $user = User::with('following', 'followers', 'posts')->where('id', $id)->first();
        return $user;
    }

    // return array of users
    public function getByName($name)
    {
        $users = User::where('name', 'like', '%'.$name.'%')->get();
        return $users;
    }

    // return user object
    public function getUserPosts($id)
    {
        $user = User::with('posts.comments')->where('id', $id)->first();
        return $user;
    }

    // return array of followers
    public function getFollowers($id)
    {
        $followers = Follower::with('user')->where('user_id', $id)->get();
        return $followers;
    }

    // return array of following
    public function getFollowing($id)
    {
        $following = Following::with('user')->where('user_id', $id)->get();
        return $following;
    }

    public function toggleFollow(Request $request)
    {
        $follow = Following::where('user_id', $request->input('user_id'))
                    ->where('following', $request->input('following'))
                    ->first();
        if(!$follow)
        {
            $follow = new Following();
            $follow->user_id = $request->input('user_id');
            $follow->following = $request->input('following');
            $follow->save();

            $follower = new Follower();
            $follower->user_id = $request->input('following');
            $follower->follower = $request->input('user_id');
            $follower->save();

            return ['status' => 'OK', 'data' => 'follow'];
        } 
        else 
        {
            $follow->delete();
            $follower = Follower::where('user_id', $request->input('following'))
                                ->where('follower', $request->input('user_id'))
                                ->delete();

            return ['status' => 'OK', 'data' => 'unfollow'];
        }
    }
}
