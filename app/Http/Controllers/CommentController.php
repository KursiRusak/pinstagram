<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use Auth;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        
        $comment = new Comment();
        $comment->post_id = $request->input('post_id');
        $comment->user_id = Auth::user()->id;
        $comment->comment = $request->input('message');
        $comment->save();

        return ['status' => 'OK'];
    }
}
