<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Like;
use App\Models\Following;
use Auth;
use DB;

class PostController extends Controller
{
    public function index()
    {
        $following = Following::where('user_id', Auth::user()->id)->get()->toArray();
        $following[] = Auth::user()->id;

        $data['feeds'] = DB::table('posts')
                            ->join('users', 'users.id', '=', 'posts.user_id')
                            ->leftjoin('comments', 'comments.post_id', '=', 'posts.id')
                            ->leftjoin('likes', 'likes.post_id', '=', 'posts.id')
                            ->select('posts.image', 'posts.created_at', 'posts.caption', 'users.id as user_id', 'users.name', 'users.email', DB::raw('SUM(likes.id) as like_count'), DB::raw('SUM(comments.id) as comment_count'))
                            ->whereIn('posts.user_id', $following)
                            ->groupBy('posts.image', 'posts.created_at', 'posts.caption', 'users.id', 'users.name', 'users.email')
                            ->orderBy('posts.created_at', 'desc')
                            ->get();

        return view('home')->with($data);
    }

    public function post($identifier)
    {
        $data['name'] = $identifier;
        $data['post'] = POST::where('image', 'like', $identifier.'.%')->first();
        return view('post')->with($data);
    }

    public function newPost(Request $request)
    {
        return view('new-post.blade.php');
    }

    public function feed(int $id)
    {
        $following = Following::select('following')->where('user_id', $id)->get()->pluck('following');
        $following[] = $id;

        $posts = DB::table('posts')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->leftjoin('comments', 'comments.post_id', '=', 'posts.id')
                    ->leftjoin('likes', 'likes.post_id', '=', 'posts.id')
                    ->select('posts.id', 'posts.image', 'posts.created_at', 'posts.caption', 'users.id as user_id', 'users.name', 'users.email', DB::raw('SUM(likes.id) as like_count'), DB::raw('SUM(comments.id) as comment_count'))
                    ->whereIn('posts.user_id', $following)
                    ->groupBy('posts.id', 'posts.image', 'posts.created_at', 'posts.caption', 'users.id', 'users.name', 'users.email')
                    ->orderBy('posts.created_at', 'desc')
                    ->get();

        return $posts;
    }

    public function getById(int $id)
    { 
        $post = Post::with('user', 'comments.user', 'likes.user')
                    ->where('id', $id)
                    ->first();
        return $post;
    }

    public function getByImageName($name)
    {
        $post = Post::with('user', 'comments.user', 'likes.user')
                        ->where('image', 'like', $name.'.%')
                        ->first();
        return $post;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:15048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/img/posts');
            $image->move($destinationPath, $name);

            $post = new Post;
            $post->user_id = Auth::user()->id;
            $post->image   = $name;
            $post->caption = $request->input('caption');
            $post->save();

            return ['status' => 'OK', 'data' => []];
        }
    }

    public function getComments($post_id)
    {
        $comments = Comment::with('users')->where('post_id', $id)->get();
        return $comments;
    }

    public function getLikes($id)
    {
        $likes = Like::with('users')->where('post_id', $id)->get();
        return $likes;
    }

    public function postLike(Request $request)
    {
        $id = $request->input('post_id');
        $user = $requst->input('user_id');
        
        $like = new Like();
        $like->post_id = $request->input('post_id');
        $like->user_id = $request->input('user_id');
        $like->save();

        return ['status' => 'OK'];
    }
}
