<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware('auth:api')->get('/user/{name}', 'UserController@getByName');


Route::get('/user/{id}', 'UserController@getById'); // v
Route::get('/user/name/{name}', 'UserController@getByName'); // v
Route::get('/user/{id}/posts', 'UserController@getUserPosts'); // v
Route::get('/user/{id}/followers', 'UserController@getFollowers'); // v
Route::get('/user/{id}/following', 'UserController@getFollowing'); // v
Route::post('/user/follow', 'UserController@toggleFollow'); // v

Route::get('/post/feed/{id}', 'PostController@feed'); // v
Route::post('/post/new', 'PostController@store'); // v
Route::get('/post/{id}', 'PostController@getById'); /// v
// Route::get('/post/image/{name}', 'PostController@getByImageName');
// Route::get('/post/{id}/comments', 'PostController@getComments');
// Route::post('/post/{id}/comments', 'PostController@postComments');
Route::post('/post/{id}/likes', 'PostController@getLikes');
// Route::post('/post/{id}/like', 'PostController@postLike');

Route::post('/comment/create', 'CommentController@store');

Route::get('/like/{id}', 'LikeController@getLike');
Route::post('/like/toggle', 'LikeController@toggleLike');