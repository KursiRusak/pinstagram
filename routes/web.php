<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['unauthenticated']], function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

Auth::routes();

Route::group(['middleware' => ['authenticated']], function () {

    Route::get('/home', 'PostController@index')->name('home');
    Route::get('/search', 'HomeController@search')->name('search');

    // Profile
    Route::get('/user/{id}', 'UserController@index')->name('user');
    // Route::get('/user/{id}/followers', 'UserController@followers')->name('followers');
    // Route::get('/user/{id}/following', 'UserController@following')->name('following');

    // Post
    Route::get('/post/{id}', 'PostController@post')->name('post');
    // Route::get('/post/new', 'PostController@newPost')->name('newPost');
    // Route::get('/post/{id}/comments', 'PostController@comments')->name('comments');
    // Route::get('/post/{id}/likes', 'PostController@likes')->name('likes');

});