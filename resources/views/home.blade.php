@extends('layouts.app')

@push('styles')
<style>
.brick-container{
    position: relative;
    width: 1140px !important;
    /* margin-right: auto; */
    /* margin-left: auto; */
}
.feed-item{
    width: 277px;
    border: 1px solid transparent;
    border-radius: 3px;
    background-color: #fff;
    margin: 0;
    padding: 0;
    padding-bottom: 
    border: none;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-transition: border-color 0.4s; /* Safari */
    transition: border-color 0.4s;
}
.brick-container .far, .brick-container .fas{
    font-size: 1.4em;
}
.card-header{
    background-color: #fff;
    padding:10px
}
.card-body{
    padding: 10px;
}
p{
    margin-bottom: 5px;
}
.soft-text{
    color: rgb(0,0,0,0.5);
    font-size: 13px;
    /* font-size: 7rem; */
}
.date-diff{
    margin-top: 15px;
}
</style>
@endpush

@section('content')
<div class="container">
    <!-- <div class="row justify-content-center"> -->
        <div class="row nm">
            <div class="brick-container" id="feed-container">
            </div>
        </div>
        <button id="new-post" class="btn btn-primary" data-toggle="modal" data-target="#new-post-modal">
            <i class="fas fa-plus"></i>
        </button>
    </div>
</div>

<div id="new-post-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-post-form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label><b>image</b></label>
                        <input type="file" class="form-control" name="image" id="image-input">
                    </div>
                    <div class="form-group">
                        <label><b>Caption</b></label>
                        <textarea class="form-control" rows="5" name="caption"></textarea>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="modal-footer">
                <button id="share-post" type="button" class="btn btn-primary">Share</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
function getFeed(id)
{
    return $.ajax({
        url: '/api/post/feed/'+id,
        type: 'GET',
    })
}

function storePost(formData)
{
    return $.ajax({
        url: '/api/post/new',
        data: formData,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false
    })
}

function shareNewPost()
{
    var formData = new FormData($('#new-post-form')[0])
    console.log(formData);
    storePost(formData).then(function(response){
        console.log(response);
        if(response.status == 'OK'){
            $('#new-post-modal').modal('hide');
            swal("", "Post has been shared", "success").then(value => {
                location.reload(); 
            });
        }
    });
}

brickView
    .on('pack',   () => {
        console.log('ALL grid items packed.');
    })
    .on('update', () => {})
    .on('resize', size => console.log('The grid has be re-packed to accommodate a new BREAKPOINT.'))

function renderPage()
{
    getFeed('{{ Auth::user()->id }}').then(response => {
        response.map((feed, key) => {
            userLink = "{{ route('user', ['id' => ':_id']) }}";
            userLink = userLink.replace(':_id', feed.user_id);
            postLink = "{{ route('post', ['id' => ':_id']) }}";
            postLink = postLink.replace(':_id', feed.image.split('.')[0]);

            $('#feed-container').append('\
                <div class="card feed-item">\
                    <div class="card-header">\
                        <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ feed.email +'"> \
                        <a href="'+ userLink +'"><b>'+ feed.name +'</b></a>\
                    </div>\
                    <a href="'+ postLink +'">\
                        <img src="img/posts/'+ feed.image +'" width="277px">\
                    </a>\
                    <div class="card-body">\
                        <i class="far fa-heart"></i>&nbsp&nbsp&nbsp<i class="far fa-comment"></i>\
                        '+ ((feed.caption)? '<p class="card-text"><b>'+ feed.name +'</b> '+ feed.caption +'</p>' : '') +'\
                        '+ ((feed.comment_count)?  '<p class="soft-text">View all '+ feed.comment_count +' comments</p>' : '') +'\
                        <div class="comment-input">\
                            <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ feed.email +'">&nbsp <span class="soft-text"><a href="'+ postLink +'#comment-input">Add a comment...</a></span>\
                        </div>\
                        <span class="date-diff soft-text">'+ Moment(feed.created_at).fromNow() +'</span>\
                    </div>\
                </div>\
            ');

            
        })
        $('img').on('load', function() {
                brickView
                    .resize(true)     // bind resize handler
                    .pack()           // pack initial items
        });
    })
}

$(document).ready(function(){
    renderPage();
    $('#new-post-modal').on('shown.bs.modal', function () {})
    $('#share-post').click(shareNewPost);
})
</script>
@endpush
