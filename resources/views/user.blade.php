@extends('layouts.app')

@push('styles')
<style>
/* #header-profile .profile-image, #header-profile .profile-name{
    display: block;
    margin: 0 auto;
} */
#header-profile{

}
.brick-container{
    position: relative;
    width: 1140px !important;
    /* margin-right: auto; */
    /* margin-left: auto; */
}
.feed-item{
    width: 278px;
    border: 1px solid transparent;
    border-radius: 3px;
    background-color: #fff;
    margin: 0;
    padding: 0;
    padding-bottom: 
    border: none;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-transition: border-color 0.4s; /* Safari */
    transition: border-color 0.4s;
}
.brick-container .far{
    font-size: 1.4em;
}
.card-header{
    background-color: #fff;
    padding:10px
}
.card-body{
    padding: 10px;
}
p{
    margin-bottom: 5px;
}
.soft-text{
    color: rgb(0,0,0,0.5);
    font-size: 13px;
    /* font-size: 7rem; */
}
#header-profile.card{
    padding: 25px;
    margin-bottom:20px;
    border: none;
}
#header-profile > *{
    display: block;
    margin: 0 auto;
}
#header-profile #profile-image{
    width: 80px;
    height:80px;
    background-color: rgb(0,0,0,0.6);
}
#header-profile #profile-name{
    margin-top: 10px;
    text-align:center;
}
#header-profile #profile-stat{
    margin-top: 8px;
    text-align:center;
}
#header-profile .profile-caption{
    margin-top: 8px;
    width: 450px;
    text-align:center;
}
.comment-input{
    margin-bottom: 8px;
}
.date-diff{
    font-size: 12.5px;
}
#follow-button-wrapper{
    margin-top: 8px;
}
.list-item{
    margin-bottom: 4px;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 no-margin no-padding">
            <div id="header-profile" class="card">
                <span id="profile-image"></span>
                <h4 id="profile-name"></h4>
                <div id="follow-button-wrapper"></div>
                <div id="profile-stat"></div>
                <div class="profile-caption">
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore"
                </div>
            </div>
            <div id="feed-container" class="brick-container"></div>
        </div>
    </div>
</div>

<div id="profile-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body"></div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

let profileUserId = JSON.parse('{{ $user }}')
let authId        = JSON.parse("{{ Auth::user()->id }}")

function getUserById(id)
{
    return $.ajax({
        url: '/api/user/'+id,
        type: 'GET',
    })
}

function getUserPosts(id)
{
    return $.ajax({
        url: '/api/user/'+id+'/posts',
        type: 'GET',
    })
}

function getFollowing(id)
{
    return $.ajax({
        url: '/api/user/'+id+'/following',
        type: 'GET',
    })
}

function getFollowers(id)
{
    return $.ajax({
        url: '/api/user/'+id+'/followers',
        type: 'GET',
    })
}

function toggleFollow(id)
{
    return $.ajax({
        url: '/api/user/follow',
        type: 'POST',
        data: {
            'user_id': "{{ Auth::user()->id }}",
            'following': id,
            '_token': '{{ csrf_token() }}'
        }
    })
}

function followedState()
{
    $('#follow-button-wrapper').html('<button id="follow-button" class="btn btn-sm btn-default"> <i class="fas fa-user-check"></i>&nbsp Followed </button>')
}

function unfollowedState()
{
    $('#follow-button-wrapper').html('<button id="follow-button" class="btn btn-sm btn-primary"> <i class="fas fa-user"></i>&nbsp Follow </button>')
}

function updateProfileStat(response)
{
    if(!response){
        getUserById(profileUserId).then(response => {
            $('#profile-stat').html('<b>'+ response.posts.length +'</b> Posts &nbsp &nbsp <a id="followers-stat" href="#" data-toggle="modal" data-target="#profile-modal"><b>'+ response.followers.length +'</b> Followers</a> &nbsp &nbsp <a id="following-stat" href="#" data-toggle="modal" data-target="#profile-modal"><b>'+ response.following.length +'</b> Following</a> ');
        });
    } else {
        $('#profile-stat').html('<b>'+ response.posts.length +'</b> Posts &nbsp &nbsp <a id="followers-stat" href="#" data-toggle="modal" data-target="#profile-modal"><b>'+ response.followers.length +'</b> Followers</a> &nbsp &nbsp <a id="following-stat" href="#" data-toggle="modal" data-target="#profile-modal"><b>'+ response.following.length +'</b> Following</a> ');
    }
    
}

function followButtonEventHandler()
{
    toggleFollow(profileUserId).then(response => {
        if(response.data === 'follow'){
            followedState();
            updateProfileStat();
        } else {
            unfollowedState();
            updateProfileStat();
        }
    })
}

function followingStatEventHandler()
{
    getFollowing(profileUserId).then(response => {
        $('#modal-title').html('Following');
        $('#modal-body').empty();
        response.map(list => {
            userLink = '{{ route("user", ["id" => ":_id"]) }}';
            userLink = userLink.replace(':_id', list.user.id); 
            $('#modal-body').append('<a href="'+ userLink +'"><div class="list-item"><img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ list.user.email +'"> <b>'+ list.user.name +'</b></div></a>')
        })
    })
}

function followersStatEventHandler()
{
    getFollowers(profileUserId).then(response => {
        $('#modal-title').html('Followers');
        $('#modal-body').empty();
        response.map(list => {
            userLink = '{{ route("user", ["id" => ":_id"]) }}';
            userLink = userLink.replace(':_id', list.user.id); 
            $('#modal-body').append('<a href="'+ userLink +'"><div class="list-item"><img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ list.user.email +'"> <b>'+ list.user.name +'</b></div></a>')
        })
    })
}

function renderPage()
{
    getUserById(profileUserId).then(response => {
        console.log(response);
        $('#profile-image').html('<img class="profile-image" src="https://api.adorable.io/avatars/80/'+ response.email +'"> ')
        $('#profile-name').html(response.name);
        if(profileUserId != authId){
            if(response.followers.find(follower => follower.follower === authId)){
                followedState();
            } else {
                unfollowedState();
            }
        }
        updateProfileStat(response);

    });

    getUserPosts(profileUserId).then(response => {

        response.posts.map((feed, key) => {
            postLink = "{{ route('post', ['id' => ':_id']) }}";
            postLink = postLink.replace(':_id', feed.image.split('.')[0]);

            $('#feed-container').append('\
                <div class="card feed-item">\
                    <div class="card-header">\
                        <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ response.email +'"> \
                        <b>'+ response.name +'</b>\
                    </div>\
                    <a href="'+ postLink +'">\
                        <img src="/img/posts/'+ feed.image +'" width="277px">\
                    </a>\
                    <div class="card-body">\
                        <i class="far fa-heart"></i>&nbsp&nbsp&nbsp<i class="far fa-comment"></i>\
                        '+ ((feed.caption)? '<p class="card-text"><b>'+ response.name +'</b> '+ feed.caption +'</p>' : '') +'\
                        '+ ((feed.comments)?  '<p class="soft-text">View all '+ feed.comments.length +' comments</p>' : '') +'\
                        <div class="comment-input">\
                            <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ response.email +'">&nbsp <span class="soft-text">Add a comment...</span>\
                        </div>\
                        <span class="date-diff soft-text">'+ Moment(feed.created_at).fromNow() +'</span>\
                    </div>\
                </div>\
            ');

            $('img').on('load', function() {
                brickView
                    .resize(true)     // bind resize handler
                    .pack()           // pack initial items
            });
        })
    });
}

$(document).ready(function(){
    renderPage();
    $('#follow-button-wrapper').on('click', '#follow-button', followButtonEventHandler);
    $('#profile-stat').on('click', '#following-stat', followingStatEventHandler)
    $('#profile-stat').on('click', '#followers-stat', followersStatEventHandler)
})
</script>
@endpush