@extends('layouts.app')

@push('styles')
<style>
.container .far, .container .fas{
    font-size: 1.4em;
}
.card-header{
    background-color: #fff;
    padding:10px
}
.card-body{
    padding: 10px;
}
.card-text{
    margin-bottom: 8px;
}
.soft-text{
    color: rgb(0,0,0,0.5);
    font-size: 13px;
    /* font-size: 7rem; */
}
#search-result{
    margin-top: 10px;
}
#sear-result list{
    margin-bottom: 8px;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Search People
                </div>
                <div class="card-body">
                    <form>
                        <input id="search-input" type="text" class="form-control" placeholder="John Doe">
                    </form>
                    <div id="search-result"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
function getUserByName(name)
{
    return $.ajax({
        url: '/api/user/name/'+name,
        type: 'GET'
    })
}

function searchEventHandler()
{
    name = $('#search-input').val();
    getUserByName(name).then(response => {
        $('#search-result').empty();
        response.map(user => {
            userLink = '{{ route("user", ["id" => ":_id"]) }}';
            userLink = userLink.replace(':_id', user.id);
            $('#search-result').append('<a href="'+ userLink +'"><div class=list><img src="https://api.adorable.io/avatars/40/'+ user.email +'"> '+ user.name +'</div></a>')
        })
    })
}

$(document).ready(function(){
    $('#search-input').keyup(searchEventHandler)
})
</script>
@endpush