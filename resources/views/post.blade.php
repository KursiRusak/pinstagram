@extends('layouts.app')

@push('styles')
<style>
.far, .fas{
    font-size: 1.4em;
}
.card-header{
    background-color: #fff;
    padding:10px
}
.card-body{
    padding: 10px;
}
.card-text{
    margin-bottom: 8px;
}
.soft-text{
    color: rgb(0,0,0,0.5);
    font-size: 13px;
    /* font-size: 7rem; */
}
.date-diff{
    margin-top: 15px;
}
#comment-input{
    width:calc(100% - 50px);
    background-color: #fff;
    border:none;
    border-bottom: 1px solid rgb(0,0,0,0.3);
}
.comment-input{
    margin-top: 8px;
}
.comment-input .avatar{
    margin-right:10px;
}
.like-button{
    cursor: pointer;
}
.like-item{
    margin-bottom: 5px;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span class="avatar-box"></span>
                    <b><span id="post-user-name"></span></b>
                </div>
                <div id="post-image">

                </div>
                <div class="card-body">
                    <i id='like' class="far fa-heart like-button"></i>&nbsp&nbsp&nbsp<i class="far fa-comment"></i>
                    <a id="likes-link" href="#" data-toggle="modal" data-target="#likes-modal">
                        <b><div id="like-box"></div></b>
                    </a>
                    <p class="card-text"><b><span id="post-user-name-caption"></span></b> <span id="post-caption"></span></p>
                    <div id="comment-box"></div>
                    <div class="comment-input">
                        <span class="avatar-box"></span>
                        <input type="text" id="comment-input" placeholder="Add a comment...">
                    </div>
                    <span class="date-diff soft-text" id="post-date-diff"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="likes-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Likes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="like-list" class="modal-body">
                <div class="like-item">
                    <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40"> <b>Ricky J.</b>
                </div>
                <div class="like-item">
                    <img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40"> <b>Ricky J.</b>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
function getPost(id)
{
    return $.ajax({
        url: '/api/post/'+id,
        type: 'GET'
    })
}

function getLikes(id)
{
    return $.ajax({
        url: '/api/like/'+id,
        method: 'GET'
    })
}

function postComment(message)
{
    return $.ajax({
        url: '/api/comment/create',
        type: 'POST',
        data: {
                'post_id': '{{ $post->id }}',
                'user_id': '{{ Auth::user()->id }}', 
                'message': message, 
                '_token': '{{ csrf_token() }}',
            }
    })
}

function toggleLike()
{
    return $.ajax({
        url: '/api/like/toggle',
        method: 'POST',
        data: {
            'post_id': '{{ $post->id }}',
            'user_id': '{{ Auth::user()->id }}', 
            '_token': '{{ csrf_token() }}',
        }
    })
}

function onShowLikeModal()
{
    $('#like-list').html('');
    getLikes('{{ $post->id }}').then(response => {
        console.log(response);
        response.map(like => {
            $('#like-list').append('<div class="like-item"><img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ like.user.email +'"> <b>'+ like.user.name +'</b></div>')
        })
    });
}

function comment(e)
{
    if (e.which == 13) {
        msg = $('#comment-input').val();
        postComment(msg).then(response => {
            $('#comment-box').append('<p class="no-margin no-padding"><b>'+ '{{ Auth::user()->name }}' +'</b> '+ msg +'</p>');
            $('#comment-input').val('');
        });
    }
}

function like()
{
    toggleLike().then(response => {
        if(response.data === 'like'){
            $('#like').removeClass('far').addClass('fas').css("color", "red");
        } else {
            $('#like').removeClass('fas').addClass('far').css("color", "inherit");
        }
    })
}

function renderPage()
{
    getPost('{{ $post->id }}').then(response => {
        $('.avatar-box').html('<img class="avatar" width="25px" height="25px" src="https://api.adorable.io/avatars/40/'+ response.user.email +'">')
        $('#post-user-name').html(response.user.name);
        if(response.likes.find(like => like.user_id == '{{ Auth::user()->id }}')){
            $('#like').removeClass('far').addClass('fas').css("color", "red");
        }
        if(response.likes.length > 0){
            $('#like-box').html(response.likes.length+' likes')
        }
        $('#post-image').html('<img src="/img/posts/'+ response.image+ '" width="100%">');
        if(response.caption)
        {
            $('#post-user-name-caption').html(response.user.name);
            $('#post-caption').html(response.caption);
        }
        response.comments.map(comment => {
            $('#comment-box').append('<p class="no-margin no-padding"><b>'+ comment.user.name +'</b> '+ comment.comment +'</p>')
        })
        $('#post-date-diff').html(Moment(response.created_at).fromNow())
    })
}

$(document).ready(function(){
    renderPage();
    $('#comment-input').keypress(comment);
    $('.like-button').click(like);
    $('#likes-link').click(onShowLikeModal)
});
</script>
@endpush
